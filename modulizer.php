<?php

require_once('./vendor/autoload.php');
use Symfony\Component\Yaml\Yaml; 

$yaml = <<<YAML

# Standard, required module info:
name: Example
description: An example module.
core: 8.x
type: module

# @todo: Others, from drupal_parse_info_file().

# Optional components of a module:
routes:
  hello_world:
    path: 'hello'
    type: page # |form|modal|dialog|ajax
    access:
      type: permission # |role|custom
      name: access content # for permissions or roles.

YAML;
go($yaml);

function go($yaml) {
  // Read YAML into something that we can work with.
  $parsed = Yaml::parse($yaml);

  // Handle .info.yml files.
  $info = get_module_info($parsed);
  $module = $info['module'];
  write_module_info($module, $info);

  // Now, let's do routes.
  $routes = get_route_info($module, $parsed);
  write_route_info($module, $routes);
}

/**
 * Extracts module .info.yml properties from parsed YAML info.
 *
 * @param array $info
 *   Parsed YAML info.
 *
 * @return array
 *   Array of module .info.yml properties.
 */
function get_module_info($info) {
  $module = array();
  $module['module'] = strtolower(str_replace(' ', '_', $info['name']));
  $module['name'] = $info['name'];
  $module['description'] = $info['description'];
  $module['core'] = $info['core'];
  $module['type'] = $info['type'];

  // @todo: other properties (e.g. dependencies), validation.
  // @todo: get all fancy and make $module a class so we document this shiz. :P

  return $module;
}

/**
 * Writes out a $module.info.yml file.
 *
 * @param string $module
 *   The module name.
 * @param array $info
 *   Parsed YAML info.
 */
function write_module_info($module, $info) {
  if (!file_exists("./$module")) {
    mkdir("./$module");
  }
  $handle = fopen("./$module/$module.info.yml", 'w');
  $yaml = Yaml::dump($info);
  fwrite($handle, $yaml);
  fclose($handle);

  // Also add a blank .module file.
  $handle = fopen("./$module/$module.module", 'w');
  fwrite($handle, '');
  fclose($handle);
}

function get_route_info($module, $info) {
  $routes = array();
  foreach ($info['routes'] as $name => $route) {
    $routes[$name] = array();
    $routes[$name]['pattern'] = '/' . $route['path'];
    switch ($route['type']) {
      case 'page':
        $routes[$name]['defaults']['_content'] = "\\Drupal\\$module\\Controller\\" . util_controller_name($module) . '::' . util_camel_case($name);
        break;
    }

    switch ($route['access']['type']) {
      case 'permission':
        $routes[$name]['requirements']['_permission'] = $route['access']['name'];
    }
  } 

  return $routes;
}

function write_route_info($module, $routes) {

  // Create the routing.yml file if it doesn't exist.
  if (!file_exists("./$module/$module.routing.yml")) {
    $handle = fopen("./$module/$module.routing.yml", 'w');
    $yaml = Yaml::dump($routes);
    fwrite($handle, $yaml);
    fclose($handle);
  }

  // Create the Controller directory if it doesn't exist.
  if (!file_exists("./$module/lib/Drupal/$module/Controller")) {
    mkdir("./$module/lib");
    mkdir("./$module/lib/Drupal");
    mkdir("./$module/lib/Drupal/$module");
    mkdir("./$module/lib/Drupal/$module/Controller");
  }

  $controller = util_controller_name($module);
  $handle = fopen("./$module/lib/Drupal/$module/Controller/$controller.php", 'w');
  $output = <<<CONTROLLER_HEAD
<?php
/**
 * @file
 * blah blah
 */

namespace Drupal\\$module\Controller;

use Drupal\Core\Controller\ControllerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
// @todo: Add additional 'use' statements here as needed.

class $controller implements ControllerInterface {

  public static function create(ContainerInterface \$container) {
    // @todo: Pull in any elements from the container that are required; e.g.
    // return new static(\$container->get('database')); 
    return new static();
  }

CONTROLLER_HEAD;

  // Loop through each route and create a method in the controller class for it.
  foreach ($routes as $name => $route) {
    $method = util_camel_case($name);
    $output .= <<<CONTROLLER_METHOD

  function $method() {
    \$content = array();

    // @todo: Replace with your own page content.
    \$content['example'] = array(
      '#markup' => t('Hello world!'),
    );

    return drupal_render(\$content);
  }

CONTROLLER_METHOD;
 
  }

  $output .= <<<CONTROLLER_FOOT

}

CONTROLLER_FOOT;

  // Now write it out.
  fwrite($handle, $output);
  fclose($handle);
}

/**
 * Takes a string and converts it to a machine name.
 */
function util_machine_name($string) {
  return strtolower(str_replace(' ', '_', $string));
}

/**
 * Takes a string with underscores and converts it to camel case.
 * Thanks, http://www.mrleong.net/97/php-underscore-to-camelcase/
 */
function util_camel_case($string) {
  $words = explode('_', strtolower($string));

  $return = '';
  foreach ($words as $word) {
    $return .= ucfirst(trim($word));
  }

  return $return;
}

function util_controller_name($module) {
  return ucfirst($module) . 'Controller';
}
